# my-enforce-workflow-project - https://www.liquibase.org/ 

This is a repository that contains a Liquibase project (H2 Database) to demonstrate how to enforce pipeline ordering using the Liquibase status command.
The order for this pipeline should be TEST > UAT > PROD. For example, if changes have not been deployed to TEST, they should not be deployed to UAT. Similarly, if changes have not been applied to UAT, they should not be applied to PROD.

The order of steps in this example are:
1. Attempt to deploy to UAT by checking the status of TEST. Deployment will not occur.
2. Attempt to deploy to PROD by checking the status of UAT. Deployment will not occur.
3. Deploy to TEST.
4. Deploy to UAT.
5. Deploy to PROD.

# Usage
1. Fork this repository so you can have your own copy. https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
2. From the browser go to your github "my-enforce-workflow-project" repository, and follow the instructions in the following page https://docs.gitlab.com/ee/ci/quick_start/
3. To run your job, click the "CI/CD" --> "Pipelines" --> select branch (for example: "master") --> "Run Pipeline".

# Notes
All Pipelines are set to be running on a Docker Images GitLab hosted runners.  Please see additional information about GitLab runners here https://docs.gitlab.com/runner/
